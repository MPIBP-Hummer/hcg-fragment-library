# hcg-fragment-library

Here the dimer fragment library with dimer fragments as input for HCG is stored.
REMD simulations of dimer fragments were performed by Johanned Betz as previously described in Pietrek et al. ( http://dx.doi.org/10.1021/acs.jctc.9b00809 )
